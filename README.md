# Helm ECR credentials to imagePullSecrets

Helm chart to update a namespaced imagePullSecret with ECR credentialsi, optionally using an AWS Role

## Use

Install chart passing your custom values, see `values.yaml`, example:
```
YOUR_CHART_NAME="ecr-credentials-to-imagepullsecrets"
helm install \
  --set aws_access_key_id=${YOUR_KEY_ID} \
  --set aws_secret_access_key=${YOUR_SECRET_KEY} \
  --set awsRegion=${YOUR_AWS_REGION}
  --set awsRole.enabled=true \
  --set awsRole.account=${YOUR_ROLE_ACCOUNT} \
  --set awsRole.name=${YOUR_ROLE_NAME} \
  ${YOUR_CHART_NAME} \
  .
```

To uninstall the chart, simply:
```
YOUR_CHART_NAME="ecr-credentials-to-imagepullsecrets"
helm uninstall ${YOUR_CHART_NAME}
```

*Note*: Generated secret won't be removed on chart uninstall, you'll have to manually remove it from cluster by example with `kubectl delete secret/ecr-credentials -n default` (matching `secretName` and `secretNamespace` values)


## Design decisions

* Use well known docker images and plain bash scripting to minimize project dependencies

## Whislist

* [ ] Improve object naming
* [ ] Publish chart packaged
* [ ] Add testing
* [ ] Extract bash scripting to its own file
* [ ] Add GitLab CI
* [ ] Automate release pipeline

## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/helm-ecr-credentials-to-imagepullsecrets).

Please log issues for any bug report, feature or support request.

## License

MIT License, see LICENSE file

## Contact

Please use the contact form on http://sbit.io
