{{/* vim: set filetype=mustache: */}}
{{/*
Create job common definition to keep things DRY.
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.jobtemplate" -}}
metadata:
  labels:
{{ include "ecr-credentials-to-imagepullsecrets.podLabels" . | indent 4 }}
spec:
  serviceAccountName: {{ include "ecr-credentials-to-imagepullsecrets.serviceAccountName" . }}
  terminationGracePeriodSeconds: 0
  restartPolicy: Never
  containers:
  - name: {{ include "ecr-credentials-to-imagepullsecrets.job" . }}
    image: {{ .Values.image.repository }}:{{ .Values.image.tag | default "latest" }}
    imagePullPolicy: {{ .Values.image.pullPolicy }}
    command:
      - "/bin/sh"
      - "-c"
      - |
        REGISTRIES="$(aws --profile ${AWS_PROFILE} ecr describe-repositories | grep repositoryUri | awk '{print $2}' | sed 's|[^"]*"||' | sed 's|/.*||' | uniq)"
        ECR_USER='AWS'
        ECR_PASSWORD="$(aws --profile ${AWS_PROFILE} ecr get-login-password)"

        ECR_AUTH="$(echo '${ECR_USER}:${ECR_PASSWORD}' | base64)"

        AUTHS=""
        for registry in $REGISTRIES; do
          AUTHS=$(echo  "$AUTHS"',"https://'"$registry"'/v1/": { "username": "'"${ECR_USER}"'", "password": "'"${ECR_PASSWORD}"'", "email": "", "auth": "'"${ECR_AUTH}"'"}')
        done
        AUTHS='{ "auths": {'"$(echo $AUTHS | sed 's/^,//')"'}}'

        # Point to the internal API server hostname
        APISERVER=https://kubernetes.default.svc
        # Path to ServiceAccount token
        SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
        # Read this Pod's namespace
        NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)
        # Read the ServiceAccount bearer token
        TOKEN=$(cat ${SERVICEACCOUNT}/token)
        # Reference the internal certificate authority (CA)
        CACERT=${SERVICEACCOUNT}/ca.crt
        # Explore the API with TOKEN
        curl -s --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api

        SECRET_NAMESPACE={{ required "A valid .Values.secretNamespace entry required!" .Values.secretNamespace }}
        SECRET_NAME={{ required "A valid .Values.secretName entry required!" .Values.secretName }}

        # ImagePull secrets must be base64 encoded
        DATA=$AUTHS
        DATA_BASE64=$(echo $DATA | base64)

        # Get current secret
        #curl -sv --cacert ${CACERT} \
        #    -X GET \
        #    -H "Authorization: Bearer ${TOKEN}" \
        #    -H 'Accept: application/json' \
        #    -H 'Content-Type: application/json' \
        #    ${APISERVER}/api/v1/namespaces/${SECRET_NAMESPACE}/secrets/${SECRET_NAME}

        # Delete current secret
        curl -sv --cacert ${CACERT} \
            -X DELETE \
            -H "Authorization: Bearer ${TOKEN}" \
            -H 'Accept: application/json' \
            -H 'Content-Type: application/json' \
            ${APISERVER}/api/v1/namespaces/${SECRET_NAMESPACE}/secrets/${SECRET_NAME}

        # Upload the new secret
        curl -sv --cacert ${CACERT} \
            -X POST \
            -d @- \
            -H "Authorization: Bearer ${TOKEN}" \
            -H 'Accept: application/json' \
            -H 'Content-Type: application/json' \
            ${APISERVER}/api/v1/namespaces/${SECRET_NAMESPACE}/secrets <<EOF
        {
         "apiVersion": "v1",
         "kind": "Secret",
         "metadata": {"namespace":"${SECRET_NAMESPACE}","name":"${SECRET_NAME}"},
         "type": "dockerconfigjson",
         "data": {".dockerconfigjson": "${DATA_BASE64}"}
        }
        EOF

    env:
      - name: AWS_ACCESS_KEY_ID
        valueFrom:
          secretKeyRef:
            name: {{ include "ecr-credentials-to-imagepullsecrets.fullname" . }}-aws-creds
            key: aws_access_key_id
      - name: AWS_SECRET_ACCESS_KEY
        valueFrom:
          secretKeyRef:
            name: {{ include "ecr-credentials-to-imagepullsecrets.fullname" . }}-aws-creds
            key: aws_secret_access_key
      - name: AWS_CONFIG_FILE
        value: "/opt/aws/config"
      - name: AWS_PROFILE
        value: {{ .Values.awsRole.profile | default "myprofile" | quote }}
      - name: AWS_DEFAULT_REGION
        value: {{ required "A valid .Values.awsRegion entry required!" .Values.awsRegion | quote }}
    volumeMounts:
      - name: {{ include "ecr-credentials-to-imagepullsecrets.fullname" . }}-aws-config
        mountPath: /opt/aws
  volumes:
  - name: {{ include "ecr-credentials-to-imagepullsecrets.fullname" . }}-aws-config
    configMap:
      # Provide the name of the ConfigMap containing the files you want
      # to add to the container
      name: {{ include "ecr-credentials-to-imagepullsecrets.fullname" . }}-aws-config
      items:
        - key: config
          path: config
{{- end }}
