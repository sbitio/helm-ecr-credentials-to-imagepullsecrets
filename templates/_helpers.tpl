{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.labels" -}}
helm.sh/chart: {{ include "ecr-credentials-to-imagepullsecrets.chart" . }}
{{ include "ecr-credentials-to-imagepullsecrets.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ecr-credentials-to-imagepullsecrets.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ecr-credentials-to-imagepullsecrets.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Pod labels
*/}}
{{- define "ecr-credentials-to-imagepullsecrets.podLabels" -}}
app.kubernetes.io/name: {{ include "ecr-credentials-to-imagepullsecrets.name" . }}
helm.sh/chart: {{ include "ecr-credentials-to-imagepullsecrets.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Define more resource names
*/}}

{{- define "ecr-credentials-to-imagepullsecrets.job" }}
{{- default (printf "%s-job" (include "ecr-credentials-to-imagepullsecrets.fullname" .)) -}}
{{- end }}

{{- define "ecr-credentials-to-imagepullsecrets.cronJob" }}
{{- default (printf "%s-cron" (include "ecr-credentials-to-imagepullsecrets.fullname" .)) -}}
{{- end }}
